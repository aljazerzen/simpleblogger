SIMPLE BLOGGER
=======

Enostavni blog

SETUP
=====

1. Get a git client:

- Debian linuxes: `$ apt-get install git`
- Others: [git downloads](http://git-scm.com/downloads)

2. Go to directory you want your repo be in (git will create new subfolder for the project)
3. `$ git clone https://aljazerzen@bitbucket.org/aljazerzen/mojster.git`

Now you have your own copy of this repo.

COMMITING
=========
1. Make changes
2. `$ git add [changed files]` - to add files
3. `$ git commit -m "[message]"` - to commit changes 
4. `$ git pull` - to see if there were any changes on the server and merge
5. `$ git push` - to commit changes 

`$ git status` - at any time to see current status of project
