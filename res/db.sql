/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50546
Source Host           : localhost:3306
Source Database       : simpleBlogger

Target Server Type    : MYSQL
Target Server Version : 50546
File Encoding         : 65001

Date: 2015-12-27 23:08:57
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for categories
-- ----------------------------
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `parentId` int(10) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (parentId) REFERENCES categories(id) ON DELETE CASCADE
) ENGINE=myISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of categories
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL UNIQUE,
  `password` varchar(128) NOT NULL,
  `email` varchar(254) NOT NULL UNIQUE,
  `lastChange` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  `activated` int(1) NOT NULL DEFAULT '0',
  `actCode` varchar(32) NOT NULL,
  `type` int(2) NOT NULL DEFAULT '1', -- 0: none, 1: user, 2: admin
  PRIMARY KEY (`id`)
) ENGINE=myISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users (username: admin, password: pass1234)
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', 'b66dd5a7a689f88e302ab2ae4a9567f9c7572c18e520b3bf712bb2630b3931a503d647baedf48df470006312d07984216578b60526e5ee6137ef1fd215190a0c', '', '0000-00-00 00:00:00', '1', '', '2');

-- ----------------------------
-- Table structure for posts
-- ----------------------------
DROP TABLE IF EXISTS `posts`;
CREATE TABLE `posts` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `uid` int(20),
  `cid` int(10) ,
  `title` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `markdown` boolean NOT NULL DEFAULT FALSE,
  `postDate` datetime NOT NULL,
  `lastChange` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  FOREIGN KEY (uid) REFERENCES users(id) ON DELETE SET NULL, 
  FOREIGN KEY (cid) REFERENCES categories(id) ON DELETE SET NULL
) ENGINE=myISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of posts
-- ----------------------------