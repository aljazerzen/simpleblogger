# Technical draft

## Files

### Reply parser `rp.php`
- gets reply from browser and does requested action
- should not be included in other php files
- gets parameters from POST request variables
- program flow
    - authorization
    - validating inputs - reqired inputs
    - permission check
    - update database
    - redirect back to website
- private array, that contains permission levels (public, user, admin) for actions

### Init page `ip.php`
- wrapper for including other php when initializing a page 

### Authorization `auth.php`
- authorization < session, POST parameters > aun, apw, aui, aut
- calls its own functions and sets global variables when included
- program flow
    - check for post parameters
    - if there are:
        - verify them in the database and get user id and type
        - write data to $_SESSION
    - otherwise check for session data
- *if there are post parameters, but they arent valid, session data will not be checked*

### InputChecker `ic.php`
- only declares functions, they have to be called after including
- checks types of inputs and removes illegal ones
- checks action and required inputs

### Permission check `perm.php`
- only declares functions, they have to be called after including
- one function
    - hasPermission: Determines whether user has perticular permission granted given global parameters
- TODO: optimize to cache data in SESSION

### Permission check - actions `permAct.php`
- only declares functions, they have to be called after including
- checks if user is allowed to perform desired action
- before calling make sure that all required global parameters are set and checked

### Permission check - pages `permPage.php`
- when called checks if user is allowed access desired page
- if not redirects him to (You should register/login) / (Forbidden) page

### Database interface `db.php`
- calls its own functions and sets global variables when included
- connect function: creates global variable storing connection
- no permission checks (assume that user is allowed to perform desired action)

### Page printer `pp.php`
- returns formated 'data blocks' *those can be tables, js code, php arrays etc.*
- only declares functions, they have to be called after including
- 3 types of functions
    - getting data from db
    - formatting data to html/js/php
    - changing format of individual fields

### Mailing interface `mail.php`
- is responsible for sending mails

### Captcha `captcha.php`
- only declares functions, they have to be called after including
- two functions:
    - void genCaptcha()
    - bool checkCaptcha()

## Procedures

- posting
- deleting posts 

## Actions

|        Name       | Permission* | Code | Parameters (#optional) beside u,p,a |
|-------------------|-------------|------|-------------------------------------|
| login             | uo          | li   |                                     |
| logout            | uo          | lo   |                                     |
| addUser           | a           | ua   | un, pw, em                          |
| activateAccount   | p           | uv   | ac                                  |
| update user       | uo, a       | uu   | uid, un, pw, em                     |
| remove user       | uo, a       | ur   | uid                                 |
| toggle admin      | a           | ut   | uid                                 |
| add post          | ua          | pa   | ti, te, cid, uid, md                |
| update post       | ua          | pu   | pid, ti, te, cid, uid, md           |
| remove post       | ua          | pr   | pid                                 |
| add category      | ua          | ca   | ti, cid(parent)                     |
| remove category   | ua          | cr   | cid                                 |
| upload file       | ua          | fa   | ti                                  |
| delete file       | ua          | fr   | ti                                  |
| update appearance | ua          | au   | json                                |
| update settings   | ua          | su   | json                                |

| Perm. code |              Comment               | Required parameters beside aui, aua |
|------------|------------------------------------|-------------------------------------|
| p          | public                             |                                     |
| a          | admin                              |                                     |
| uo         | user owner                         |                                     |
| ua         | user owner (activated)             |                                     |

| Param. code |   Param. name   |                  Comment                   |
|-------------|-----------------|--------------------------------------------|
| a           | action code     |                                            |
| u           | username        |                                            |
| p           | password        |                                            |
| ac          | activation code |                                            |
| uid         | user id         | If not present, id of current user is used |
| pid         | post id         |                                            |
| cid         | category id     |                                            |
| ti          | title           |                                            |
| te          | text            |                                            |
| un          | username        |                                            |
| pw          | password        |                                            |
| em          | email           |                                            |
| md          | is markdown     |                                            |
| json        | json string     |                                            |

*User has public permissions, admin has user permissions*

## Styling (Themes)

There is a file `css/main.css` that contains all general styling for the site.
`css/` directory also contains other styling files for individual pages.

To implement customization, website actually uses file `data/custom-main.css`. This file is complied when user changes website appearance settings (via an action). To assemble it, variable names in square brackets in `css/main.css` (from the table below) are replaced with custom values that are extracted from `data/custom.json`;
For example [cl_body] in `css/main.css` is replaced with value from `data/custom.json` and written in `data/custom-main.css`.

### Custom values

| JSON Variable name |
|--------------------|
| cl_body            |
| cl_background      |
| cl_line            |
| cl_text            |
| cl_headers         |
| cl_selected        |
| cl_subtext         |
| line_width         |
| font_default       |
| font_header        |
| container_width    |
| nav_padding        |

## Settings

In JSON file, that stores general information about the site.

| JSON variable name |         Comment         |
|--------------------|-------------------------|
| blogName           |                         |
| navbar             | array of tabs in navbar |
| > title            | title displayed         |
| > link             | link to page            |
| fonts              | array of custom fonts   |
| > name             |                         |
| > url              |                         |


