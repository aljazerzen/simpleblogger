<nav class="container navbar navbar-default" id="main-navbar"> <!-- navbar-fixed-top -->

	<div class="navbar-inner">

		<div class="navbar-head">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse-main">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand navheader-brand" href="."><?php echo $settings['blogName'];?></a>
		</div>

		<div class="collapse navbar-collapse navbar-collapse-main" id="navbar-collapse">
			<ul class="nav navbar-nav navbar">
				
				<?php
					if($controlPanel==true) {
						echo "<li class='active'><a href='#'>Control panel</a></li>";
						echo "<li><a href='.'>Back to page</a></li>";
					} else {
						
						$link = preg_replace("|^.*/|", "", $_SERVER["REQUEST_URI"]);
						echo arrayToTemplate($settings['navbar'],"<li{selected}><a href='{url}'>{title}</a></li>"," class='active'",$link,"url");
					}
				?>
			</ul>		
		</div>
	</div>
</nav>
<?php echo $notification?>