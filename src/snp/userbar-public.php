- <span class="dropup" >
	<a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">Login</a>
	<ul class="dropdown-menu dropdown-menu-right" role="menu">
		<li>
			<div id="login-form-container">
				<form class="form-horizontal col-sm-10 col-sm-offset-1" action='php/rp.php' method='POST'>
					<input type="hidden" name="a" value="li">
					<div class="form-group">
						<label for="login-un">Username</label>
						<input type="text" class="form-control input-sm" name="u" placeholder="Username" id="login-un">
					</div>
					<div class="form-group">
						<label for="login-pw">Password</label>
						<input type="password" class="form-control input-sm" name="p" placeholder="Password" id="login-pw">
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-default btn-sm">Login</button>
					</div>
				</form>	
			</div>
		</li>
	</ul>
</span>
