<?php 

if(isset($cid)) {
	$categoryPathArray = array_reverse(toCategoryPathArray(indexesToCids(resToArray(dbGetCategoryList())),$cid));
	$subCategories=resToArray(dbGetCategoryListOf($cid));

		
	if(count($categoryPathArray)>1 || count($subCategories)>0) {

		echo '<div class="category-bar inline">';

		$categoryBar = arrayToTemplate( $categoryPathArray,
			'<a href="posts.php?ti={name}&cid={id}" class="btn btn-default">{name}</a> > ');

		$subCategorySelector="";

		if(count($subCategories)>0) {
			$subCategorySelector = '<div class="dropdown">
						<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">'.
			arrayToTemplate($subCategories,"<li><a href='posts.php?ti={name}&cid={id}'>{name}</a></li>").
			'				</ul>
					</div>';
		} else {
			if($categoryBar) {
				$categoryBar = substr($categoryBar, 0, -3);
			}
		}

		echo $categoryBar;
		echo $subCategorySelector;
		echo "</div>";
	}
}
?>
