<div class="col-sm-3 navbar-vertical">
	<div class="collapse navbar-collapse navbar-collapse-main">
		<ul class="nav nav-pills">

			<li data-toggle="collapse" data-target="#postsSubmenu"><a>Posts</a></li>

			<ul class="nav nav-pills subnav collapse<?php if($page=='managePosts' || $page=='editPost' || $page=='newPost') echo ' in';?>" id="postsSubmenu">
				<li <?php if($page=='newPost') echo ' class="active"';?>><a href="editPost.php">New</a></li>
				<li <?php if($page=='managePosts' || $page=='editPost') echo ' class="active"';?>><a href="managePosts.php">Manage</a></li>
			</ul>

			<li <?php if($page=='manageCategories') echo ' class="active"';?>><a href="manageCategories.php">Categories</a></li>
			<li <?php if($page=='manageFiles') echo ' class="active"';?>><a href="manageFiles.php">Files</a></li>
			<li <?php if($page=='manageAppearance') echo ' class="active"';?>><a href="manageAppearance.php">Appearance</a></li>
			<li <?php if($page=='manageSettings') echo ' class="active"';?>><a href="manageSettings.php">Settings</a></li>

			<?php 
			$active = ($page=='manageUsers'?" class='active'":"");
			if($aut==2) echo "
				<li$active><a href='manageUsers.php'>Users</a></li>";?>

			<li <?php if($page=='profile') echo ' class="active"';?>><a href="profile.php">Profile</a></li>
		</ul>
	</div>
</div>