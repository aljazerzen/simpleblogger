
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="res/bootstrap.min.css" rel="stylesheet">
<link href="data/custom-main.css" rel="stylesheet">
<link rel="shortcut icon" href="data/icon.png">
<!-- <link href="css/main.css" rel="stylesheet"> -->

<style>
<?php
if($controlPanel==6)
	echo "
	@media (min-width: 768px) {
		.container {
		    width: 768px;
		}
	}
	@media (min-width: 940px) {
		.container {
		    width: 940px;
		}
	}";
?>

<?php
$settings = getJSONfile("data/settings.json",true);
?>

</style>