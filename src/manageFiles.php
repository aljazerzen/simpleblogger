<?php $page="manageFiles"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Files - <?php echo $settings['blogName'];?></title>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9">

			<table class="table table-hover">
				<thead>
					<tr><th>Preview</th><th>Name</th><th>Delete</th></tr>
				</thead>
				<tbody>
					<?php echo arrayToTemplate(addPreviewToImages(getFileList(), 
						'<img class="image-preview" src="{src}"></img>'),
					'<tr class="fileRow" value="{name}" title="{name}"><td>{preview}</td><td>{name}</td><td class="action-cell"><a class="btn btn-default glyphicon glyphicon-remove"></a></td></tr>');
					?>
				</tbody>
			</table>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Upload new file</h1>	
				</div>
				<div class="panel-body">
					<form class="form-inline" action="php/rp.php" method="POST" enctype="multipart/form-data" id="newFileForm">
						<input type="hidden" name="a" value="fa"/>
						<div class="form-group">
							<label for="title" class="control-label">Name</label>
							<input type="text" id="title" name="ti" class="form-control"/>
						</div>
						<div class="form-group">
							<span class="btn btn-default btn-file">
								<span>Open</span>
								<input type='file' name="file" id="inputNewFile"/>
							</span>
						</div>
						<div class="form-group">
							<input type="submit" class="form-control" value="Add"/>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<form class="hidden" action="php/rp.php" method="POST" id="deleteForm"> 
		<input type="hidden" name="a" value="fr"/>
		<input type="hidden" name="ti" id="ti"/>
	</form>

	<?php include "snp/footer.php" ?>
	<script src="js/manageFiles.js"></script>

</body>
</html>