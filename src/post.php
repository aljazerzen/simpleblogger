<?php $page = "post";
require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php include "snp/header.php"; ?>
    <?php include "snp/postHeader.php" ?>
    <?php
    $postArray = formatArray(resToArray(dbGetPost($pid)));
    if (isset($postArray[0])) {
        echo "<title>" . $postArray[0]["title"] . " - " . $settings["blogName"] . "</title>";
    } else {
        echo "<title>Not found</title>";
    }
    ?>
</head>
<body>
<?php include "snp/navbar.php" ?>

<div class="container">

    <?php
    echo arrayToTemplate($postArray, $postTemplate); ?>
</div>

<?php include "snp/footer.php" ?>

</body>
</html>