<?php $page="homepage"; require "php/ip.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<?php include "snp/postHeader.php" ?>	
	<?php
	echo "<title>".$settings["blogName"]."</title>";
	?>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">
<?php
$parts = parse_url($settings['homepage']);
parse_str($parts['query'], $params);
foreach ($params as $key => $value) {
	$GLOBALS[$key]=$value;
}
echo postQuery(false);

?>
	</div>

	<?php include "snp/footer.php" ?>
		
</body>
</html>