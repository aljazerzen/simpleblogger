$().ready(function (){
	$(".fileRow .glyphicon-remove").click(function () {
		$("#ti").val($(this).parent().parent().attr("value"));
		$("#deleteForm").submit();
		stop=true;
	});
	$("#inputNewFile").change(function () {
		var name=$(this)[0].files[0].name;
		$("#title").val(name);
		$(this).prev().html(name);
	});
});