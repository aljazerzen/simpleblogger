$(function(){
	$("#save-button").click(function(){
		var jsonData = {};
		var inputs = $("#jsonData input");
		for (var i = 0; i < inputs.length; i++) {
		 	jsonData[inputs[i].id]=inputs[i].value;
		}; 
		var navtabs = $("#nav-tabs .nav-tab");
		jsonData.navbar = [];
		for (var i = 0; i < navtabs.length; i++) {
			jsonData.navbar[i] = {};
			jsonData.navbar[i].url = navtabs.eq(i).data('link');
			jsonData.navbar[i].title = jsonData.navbar[i].url.replace(/^.*ti=/,'').replace(/&.*$/,'');
		};
		jsonData.homepage = $("#homepage .nav-tab").data('link');
		var fonttabs = $("#font-tabs .font-tab");
		jsonData.fonts = [];
		for (var i = 0; i < fonttabs.length; i++) {
			jsonData.fonts[i] = {};
			jsonData.fonts[i].url = fonttabs.eq(i).data('url');
			jsonData.fonts[i].name = jsonData.fonts[i].url.replace(/^.*\//,'').replace(/[.].*$/,'');
		};


		$("#form-json-field").val(JSON.stringify(jsonData));
		$("#save-form").submit();
	});

	$(".fontRow .glyphicon-ok").click(function (){
		addFontElement($(this).parent().parent().attr("value"));

	});	

	function addFontElement(url) {
		var title = url.replace(/^.*\//,'').replace(/[.].*$/,'');
		$("#font-tabs").append("<div class='font-tab btn-default' data-url='"+url+"'><span>"+title+"</span><span class='glyphicon glyphicon-remove'></span></div>");
		$("#font-tabs").children().last().find(".glyphicon-remove").click(function (){
			$(this).parent().remove();
		});
	}

	$("#add-nav-tab").click(function (){
		addLinkElement("");
		openEditLinkModal($("#nav-tabs").children().last());
	});

	function addLinkElement(link) {
		var title = link.replace(/^.*ti=/,'').replace(/&.*$/,'');
		$("#nav-tabs").append("<div class='nav-tab btn btn-default' data-link='"+link+"'><span>"+title+"</span><span class='glyphicon glyphicon-remove'></span></div>");
		$("#nav-tabs").children().last().click(function (){
			openEditLinkModal($(this));
		});
		$("#nav-tabs").children().last().find(".glyphicon-remove").click(function (){
			$(this).parent().remove();
		});
	}

	function loadLinkElement(linkElement, link) {
		var title = link.replace(/^.*ti=/,'').replace(/&.*$/,'');
		linkElement.data('link',link);
		linkElement.children().first().html(title);
	}

	function openEditLinkModal(linkElement) {
		var link = linkElement.data('link');
		var data = link.replace(/^.*[?]/,'').split("&");
		var dataAsoc = {};
		for (var i = 0; i < data.length; i++) {
			var field = data[i].split("=");
			dataAsoc[field[0]] = field[1]; 
		};

		var inputs = $("#editLinkModal input,#editLinkModal select");
		for (var i = 0; i < inputs.length; i++) {
		 	if(dataAsoc[inputs[i].id]) {
		 		if(inputs[i].type == 'checkbox')
		 			inputs[i].checked = dataAsoc[inputs[i].id] != "" && dataAsoc[inputs[i].id] != undefined;
				else
		 			inputs[i].value = dataAsoc[inputs[i].id];
		 	} else {
		 		inputs[i].value = "";
		 		inputs[i].checked = false;
		 	}
		}

		$("#editLinkModal").modal('show');
		$("#editLinkModalAddButton").off('click').on('click',function (){
			var link = "posts.php?";
			var inputs = $("#editLinkModal input,#editLinkModal select");
			for (var i = 0; i < inputs.length; i++) {
			 	if(inputs[i].value) {
				 	link += inputs[i].id + "=" + inputs[i].value +"&";
				} else if(inputs[i].checked) {
					link += inputs[i].id + "=1&";
				}
			}
			link = link.substring(0,link.length-1);
			loadLinkElement(linkElement,link);
			$("#editLinkModal").modal('hide');
			return false;
		});
	}

	function setJsonData(jsonData) {
		if(jsonData) {
			var inputs = $("#jsonData input");
			for (var i = 0; i < inputs.length; i++) {
			 	if(jsonData[inputs[i].id]) {
			 		inputs[i].value = jsonData[inputs[i].id];
			 	}
			}
			if(jsonData.navbar) {
				for (var i = 0; i < jsonData.navbar.length; i++) {
					addLinkElement(jsonData.navbar[i].url);
				};
			}
			if(jsonData.homepage) {
				$("#homepage").append("<div class='nav-tab btn btn-default' data-link='"+jsonData.homepage+"'><span>Set homepage</span></span></div>");
				$("#homepage").children().last().click(function (){
					openEditLinkModal($(this));
				});
			}
			if(jsonData.fonts) {
				for (var i = 0; i < jsonData.fonts.length; i++) {
					addFontElement(jsonData.fonts[i].url);
				};
			}
		}

	}

	$.ajax({
	    url: 'data/settings.json',
	    type: 'POST',
	    success: function(data) {
	        setJsonData(data);
	    }
	});

});

