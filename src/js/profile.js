$('.dataNew').hide();

function editProfile() {
	$('.dataOld').hide();
	$('.dataNew').show();	
	$('#saveButton').collapse("toggle");
	$('#cancelButton').collapse("toggle");
	$('#editButton').collapse("toggle");
}

function cancelEdit() {
	$('.dataOld').show();
	$('.dataNew').hide();	
	$('#saveButton').collapse("toggle");
	$('#cancelButton').collapse("toggle");
	$('#editButton').collapse("toggle");
}

function saveProfile() {
	$("#profileForm").submit();
}