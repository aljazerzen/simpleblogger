$(function(){

	$('.color-input').colorpicker();

	$("#save-button").click(function(){
		var jsonData = {};
		var inputs = $("#jsonData input");
		for (var i = 0; i < inputs.length; i++) {
		 	jsonData[inputs[i].id]=inputs[i].value;
		}; 

		$("#form-json-field").val(JSON.stringify(jsonData));
		$("#save-form").submit();
	});

	$("#file-json").change(function (){
		var files = $(this)[0].files;
		console.log(files);
		if (files && files[0]) {
			var reader = new FileReader();
			reader.onload = function (e) {
				setJsonData(JSON.parse(e.target.result));
			}
			reader.readAsText(files[0]);
		}
	});

	function setJsonData(jsonData) {
		if(jsonData) {
			var inputs = $("#jsonData input");
			for (var i = 0; i < inputs.length; i++) {
			 	if(jsonData[inputs[i].id]) {
			 		if(inputs[i].parentNode.classList.contains("color-input")) {
			 			$(inputs[i].parentNode).colorpicker('setValue', jsonData[inputs[i].id]);
			 		} else {
				 		inputs[i].value = jsonData[inputs[i].id];
				 	}
			 	}
			}
		}
	}

	$.ajax({
	    url: 'data/theme.json',
	    type: 'POST',
	    success: function(data) {
	        setJsonData(data);
	    }
	});

});

