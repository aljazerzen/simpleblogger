<?php $page="editPost"; require_once "php/ip.php"; 

if($page=="newPost") {
	$post=array('id'=>'','title'=>'','text'=>'','cid'=>'','markdown'=>true);
} else {
	$post=resToArray(dbGetPost($pid))[0];	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title><?php 

if($page=="newPost") {
	echo "New post";
} else {
	echo "Edit post";
}
echo " - ".$settings['blogName'];
	 ?></title>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container ">

		<?php include "snp/editNavbar.php" ?>		

		<form class="form-horizontal col-sm-9" method="POST" action="php/rp.php">
			<input type='hidden' name="a" value="<?php echo ($post['id']!=''?'pu':'pa');?>"/>
			<input type='hidden' name="pid" value="<?php echo $post['id'];?>"/>

			<div class='form-group'>
				<label for='ti' class="col-sm-2 control-label">Title</label>
				<div class='col-sm-10'>
					<input type='text' class='form-control' name="ti" id="ti" value="<?php echo $post['title'];?>"/>
				</div>
			</div>
			<div class='form-group'>
				<label for='te' class="col-sm-2 control-label">Text</label>
				<div class='col-sm-10'>
					<textarea class='form-control' name="te" id="te" rows="20"><?php echo $post['text'];?></textarea>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-10 col-sm-offset-2'>
					<button type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Insert image</button>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-10 col-sm-offset-2'>
					<div class="checkbox">
						<label>
							<input type='checkbox' name="md" id="md" <?php echo $post['markdown']?'checked':''?>/>Markdown
						</label>
					</div>
				</div>
			</div>
			<div class='form-group'>
				<label for='cid' class="col-sm-2 control-label">Category</label>
				<div class='col-sm-10'>
					<select class='form-control' name="cid" id="cid">
						<?php 
						echo arrayToOptions(categoriesToFullPath(indexesToCids(resToArray(dbGetCategoryList()))),'id','path',$post['category'],'name');
						?>
					</select>
				</div>
			</div>
			<div class='form-group'>
				<div class='col-sm-4 col-sm-offset-2'>
					<input type='submit' class='btn btn-default' value="Submit" />
				</div>
			</div>
		</form>	
	</div>

	<?php include "snp/footer.php" ?>
	<script src="res/js/tinymce/tinymce.min.js"></script>
	<script>

		function toggleEditors() {
			if($("#md").prop('checked')) {
				if(tinyMCE.editors[0])
					tinyMCE.editors[0].remove();
			} else {
				tinymce.init({ 
					selector:'#te',
					statusbar: false,
					toolbar: 'undo redo styleselect bold italic alignleft aligncenter alignright bullist numlist outdent indent code',
					plugins: 'code',
					editor_deselector : "no_mce",
					setup: function (ed) {
						ed.on('init', function(args) {
							toggleEditors();
						});
					}
				});
			}
		}

		$(function () {
			$("#md").change(toggleEditors);
		});

		function fileSelected(path) {
			insertAtCaret("te", "<img src='"+path+"'></img>\n");
		}

		function insertAtCaret(areaId,text) {
			var txtarea = document.getElementById(areaId);
			var scrollPos = txtarea.scrollTop;
			var strPos = 0;
			var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ? 
				"ff" : (document.selection ? "ie" : false ) );
			if (br == "ie") { 
				txtarea.focus();
				var range = document.selection.createRange();
				range.moveStart ('character', -txtarea.value.length);
				strPos = range.text.length;
			}
			else if (br == "ff") strPos = txtarea.selectionStart;

			var front = (txtarea.value).substring(0,strPos);  
			var back = (txtarea.value).substring(strPos,txtarea.value.length); 
			txtarea.value=front+text+back;
			strPos = strPos + text.length;
			if (br == "ie") { 
				txtarea.focus();
				var range = document.selection.createRange();
				range.moveStart ('character', -txtarea.value.length);
				range.moveStart ('character', strPos);
				range.moveEnd ('character', 0);
				range.select();
			}
			else if (br == "ff") {
				txtarea.selectionStart = strPos;
				txtarea.selectionEnd = strPos;
				txtarea.focus();
			}
			txtarea.scrollTop = scrollPos;
		}
	</script>


	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="myModalLabel">Select image</h4>
				</div>
				<div class="modal-body">
					<table class="table">
						<?php echo arrayToTemplate(addPreviewToImages(getFileList(), 
							'<img class="image-preview" src="{src}"></img>',true),
						'<tr class="imageRow" value="{path}"><td>{preview}</td><td>{name}</td><td class="action-cell"><a class="btn btn-default glyphicon glyphicon-ok" data-dismiss="modal"></a></td></tr>');
						?>
					</table>
					
					<script type="text/javascript">
						$().ready(function (){
							$(".imageRow .glyphicon-ok").click(function (){
								fileSelected($(this).parent().parent().attr("value"));
							});	
						});
					</script>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

</body>
</html>