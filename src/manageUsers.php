<?php $page="manageUsers"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Users - <?php echo $settings['blogName'];?></title>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9 ">

			<table class="table table-hover">
				<thead>
					<tr><th>Username</th><th>Email</th><th>Type</th><th>Remove</th></tr>
				</thead>
				<tbody>
					<?php echo arrayToTemplate(formatArray(resToArray(dbGetUserList())),'
					<tr class="postRow" value="{id}"><td>{username}</td><td>{email}</td><td><div class="btn btn-default toggle-admin">{type}</div></td><td class="action-cell"><a class="btn btn-default glyphicon glyphicon-remove"></a></td></tr>');?>
				</tbody>
			</table>

			<span class="glyphicon glyphicon-plus btn btn-default" data-toggle="collapse" data-target="#newUserForm" style="margin-bottom:20px"></span>
			<div class="panel panel-default collapse" id="newUserForm">
				<div class="panel-body">
					<form class="form-horizontal" action="php/rp.php" method="POST">
						<input type="hidden" name="a" value="ua"/>

						<div class="form-group">
							<label for="username" class="control-label col-md-4">Username</label>
							<div class="col-md-8">
								<input type="text" id="username" name="un" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="control-label col-md-4">Email</label>
							<div class="col-md-8">
								<input type="email" id="email" name="em" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
							<label for="password" class="control-label col-md-4">Password</label>
							<div class="col-md-8">
								<input type="password" id="password" name="pw" class="form-control"/>
							</div>
						</div>
						<div class="form-group">
						<div class="col-md-offset-4 col-md-4">
								<input type="submit" class="form-control" value="Add"/>
							</div>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<form class="hidden" action="php/rp.php" method="POST" id="actionForm"> 
		<input type="hidden" name="a" id="a"/>
		<input type="hidden" name="uid" id="uid"/>
	</form>

	<?php include "snp/footer.php" ?>
	<script src="js/manageUsers.js"></script>

</body>
</html>