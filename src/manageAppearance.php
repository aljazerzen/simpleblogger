<?php $page="manageAppearance"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Appearance - <?php echo $settings['blogName'];?></title>
	<link rel="stylesheet" type="text/css" href="res/bootstrap-colorpicker.min.css">
</head>

<style type="text/css">
	
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}

</style>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9">

			<div class="margin-bottom">

				<a class="btn btn-default" href="data/custom.json" download="simpleBloggerTheme.json">Download current theme</a>
				
				<div class="btn btn-default btn-file">Load theme file
					<input type="file" id="file-json">
				</div>

				<div class="btn btn-default" id="save-button">Save</div>
			</div>

			<div class="form-horizontal" id="jsonData">

				<div class="form-group">
					<label for="cl_body" class="col-sm-3 control-label">cl_body</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_body" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<label for="cl_background" class="col-sm-3 control-label">cl_background</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_background" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="cl_line" class="col-sm-3 control-label">cl_line</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_line" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>

					<label for="cl_text" class="col-sm-3 control-label">cl_text</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_text" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="cl_headers" class="col-sm-3 control-label">cl_headers</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_headers" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>

					<label for="cl_selected" class="col-sm-3 control-label">cl_selected</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_selected" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="cl_subtext" class="col-sm-3 control-label">cl_subtext</label>
					<div class="col-sm-3">
						<div class="input-group color-input">
							<input type="text" id="cl_subtext" class="form-control">
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>

					<label for="line_width" class="col-sm-3 control-label">line_width</label>
					<div class="col-sm-3">
						<input type="text" id="line_width" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="font_default" class="col-sm-3 control-label">font_default</label>
					<div class="col-sm-3">
						<input type="text" id="font_default" class="form-control">
					</div>

					<label for="font_header" class="col-sm-3 control-label">font_header</label>
					<div class="col-sm-3">
						<input type="text" id="font_header" class="form-control">
					</div>
				</div>
				<div class="form-group">
					<label for="container_width" class="col-sm-3 control-label">container_width</label>
					<div class="col-sm-3">
						<input type="text" id="container_width" class="form-control">
					</div>

					<label for="nav_padding" class="col-sm-3 control-label">nav_padding</label>
					<div class="col-sm-3">
						<input type="text" id="nav_padding" class="form-control">
					</div>
				</div>
			</div>


		</div>
	</div>

	<form action="php/rp.php" method="post" id="save-form">
		<input type="hidden" name="a" value="au"/>
		<input type="hidden" name="json" id="form-json-field">
	</form>

	<?php include "snp/footer.php" ?>
	<script src="res/js/bootstrap-colorpicker.min.js"></script>
	<script src="js/manageAppearance.js"></script>
</body>
</html>