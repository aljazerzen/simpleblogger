<?php $page="managePosts"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Posts - <?php echo $settings['blogName'];?></title>
	<style type="text/css">
		.action-cell> *{
			/*margin-left: 15px;*/
		}
	</style>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9">
			<table class="table table-hover">
				<thead>
					<tr><th>Title</th><th>User</th><th>Category</th><th>PostDate</th><th>Edit</th><th>Delete</th></tr>
				</thead>
				<tbody>
					<?php echo arrayToTemplate(resToArray(dbGetPosts()),'
					<tr class="postRow" value="{id}"><td>{title}</td><td>{user}</td><td>{category}</td><td>{postDate}</td><td class="action-cell"><a href="editPost.php?pid={id}" class="btn btn-default glyphicon glyphicon-edit"></a></td><td><a class="btn btn-default glyphicon glyphicon-remove"></a></td></tr>');?>
				</tbody>
			</table>
		</div>
	</div>

	<form class="hidden" action="php/rp.php" method="POST" id="deleteForm"> 
		<input type="hidden" name="a" value="pr"/>
		<input type="hidden" name="pid" id="pid"/>
	</form>

	<?php include "snp/footer.php" ?>
	<script src="js/managePosts.js"></script>

</body>
</html>