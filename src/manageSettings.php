<?php $page="manageSettings"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Settings - <?php echo $settings['blogName'];?></title>
	<link rel="stylesheet" type="text/css" href="res/bootstrap-colorpicker.min.css">
	<style type="text/css">
	.nav-tab,
	.font-tab{
		margin-right: 10px; 
	}
	.nav-tab span+span,
	.font-tab span+span{
		margin-left: 5px; 
	}

	</style>
</head>

<style type="text/css">
	
	.btn-file {
		position: relative;
		overflow: hidden;
	}
	.btn-file input[type=file] {
		position: absolute;
		top: 0;
		right: 0;
		min-width: 100%;
		min-height: 100%;
		font-size: 100px;
		text-align: right;
		filter: alpha(opacity=0);
		opacity: 0;
		outline: none;
		background: white;
		cursor: inherit;
		display: block;
	}

</style>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9">

			<div class="form-horizontal" id="jsonData">

				<div class="form-group">
					<label for="blogName" class="col-sm-3 control-label">Blog name</label>
					<div class="col-sm-9">
						<input type="text" id="blogName" class="form-control" autocomplete="off">
					</div>
				</div>
				<div class="form-group">
					<label for="hp" class="col-sm-3 control-label">Homepage</label>
					<div class="col-sm-9" id="homepage">
					</div> 
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Navbar tabs</label>
					
					<div class="col-sm-9 inline">
						<div id="nav-tabs">
						</div>
						<div class="btn btn-default glyphicon glyphicon-plus" id="add-nav-tab"> </div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-3 control-label">Custom fonts</label>
					
					<div class="col-sm-9 inline">
						<div id="font-tabs">
						</div>
						<div class="btn btn-default glyphicon glyphicon-plus" id="add-font-tab" data-toggle="modal" data-target="#addFont"> </div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-3 col-sm-3">
						<div class="btn btn-default form-control" id="save-button">Save</div>
					</div>
				</div>
			</div>

		</div>
	</div>

	<form action="php/rp.php" method="post" id="save-form">
		<input type="hidden" name="a" value="su"/>
		<input type="hidden" name="json" id="form-json-field">
	</form>

	<?php include "snp/footer.php" ?>
	<script src="res/js/bootstrap-colorpicker.min.js"></script>
	<script src="js/manageSettings.js"></script>

	<div class="modal fade" id="editLinkModal" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
					<h4 class="modal-title">Page settings</h4>
				</div>
				
				<div class="modal-body form-horizontal" id="postSelector">
					<div class="form-group">
						<label for="ti" class="col-sm-3 control-label">Title</label>
						<div class="col-sm-9">
							<input type="text" id="ti" class="form-control" reqired autocomplete="off">
						</div>
					</div>
					<div class="form-group">
						<label for="cid" class="col-sm-3 control-label">Category</label>
						<div class="col-sm-9">
							<select class='form-control' name="cid" id="cid">
								<option value="">Any</option>
								<?php 
								echo arrayToOptions(categoriesToFullPath(indexesToCids(resToArray(dbGetCategoryList()))),'id','path','id',0);
								?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="uid" class="col-sm-3 control-label">User</label>
						<div class="col-sm-9">
							<select class='form-control' name="uid" id="uid">
								<option value="">Any</option>
								<?php 
								echo arrayToOptions(resToArray(dbGetUserList()),'id','username');
								?>
							</select>						
						</div>
					</div>
					<div class="form-group">
						<label for="df" class="col-sm-3 control-label">Date from</label>
						<div class="col-sm-9">
							<input type="date" id="df" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="dt" class="col-sm-3 control-label">Date to</label>
						<div class="col-sm-9">
							<input type="date" id="dt" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<label for="oi" class="col-sm-3 control-label">Order by</label>
						<div class="col-sm-9">
							<select class="form-control" id="oi">
								<option value="1">Newest first</option>
								<option value="2">Oldest first</option>
								<option value="3">Last changed first</option>
								<option value="4">Last changed last</option>
								<option value="5">Alphabetical</option>
								<option value="6">Alphabetical reversed</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="cn" class="col-sm-3 control-label">Limit</label>
						<div class="col-sm-3">
							<input type="number" id="cn" class="form-control">
						</div>
						<label for="st" class="col-sm-3 control-label">Start at</label>
						<div class="col-sm-3">
							<input type="number" id="st" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-10">
							<div class="checkbox">
								<label>
									<input type="checkbox" id="shorten"> Shorten posts
								</label>
							</div>
						</div>
					</div>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-default" id="editLinkModalAddButton">Ok</button>
				</div>
			</div>
		</div>
	</div>


	<div class="modal fade" id="addFont" tabindex="-1" role="dialog">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
				</div>
				
				<div class="modal-body" id="fontSelector">
					<table class="table">
						<?php echo arrayToTemplate(filterFiles(getFileList(),array('ttf')),
						'<tr class="fontRow" value="{path}"><td>{name}</td><td class="action-cell"><a class="btn btn-default glyphicon glyphicon-ok" data-dismiss="modal"></a></td></tr>');
						?>
					</table>				
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>

</body>
</html>