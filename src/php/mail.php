<?php

function sendActivationMail($username, $email, $actCode) {
	require_once "pp.php";

	$settings = getJSONfile("../data/settings.json");

	$array = array(array(
		"username" => $username,
		"email" => $email,
		"actCode" => $actCode,
		"siteURL" => $_SERVER['HTTP_HOST'] . preg_replace ( "|/php/.*|" , "", $_SERVER['REQUEST_URI']),
		"blogTitle" => $settings->blogName));

	ob_start();
	require("../snp/mail_activate.html");
	$template=ob_get_clean();
	$body = arrayToTemplate($array,$template);

	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer;	

	$mail->setFrom('simpleBlogger@erzen.com', 'Simple Blogger');
	$mail->addAddress($email, $username); 

	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'Welcome to Simple Blogger';
	$mail->Body    = $body;
	//$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	$mail->send();
}

?>