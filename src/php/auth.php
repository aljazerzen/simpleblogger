<?php

require_once "ic.php";
require_once 'db.php';

function checkSession() {
	if(isset($_SESSION["aun"]) && isset($_SESSION["apw"]) && isset($_SESSION["aui"]) && isset($_SESSION["aut"]) && isset($_SESSION["aua"])) {
		$GLOBALS['aun'] = $_SESSION["aun"];
		$GLOBALS['apw'] = $_SESSION["apw"];
		$GLOBALS['aui'] = $_SESSION["aui"];
		$GLOBALS['aut'] = $_SESSION["aut"];
		$GLOBALS['aua'] = $_SESSION["aua"];
		return true;
	}
	return false;
}

function checkPost() {
	$username = validatePost('u');
	$password = validatePost('p');

	if($username!=false && $password!=false) {
		$GLOBALS['aun'] = $username;
		$GLOBALS['apw'] = $password;
		return true;
	}
	return false;
}

/*
Checks if user with given username and password exists and gets his id, type, activated state
*/
function verify() {
	global $con,$aun,$apw; 

	if($row = $con->query("SELECT `id`, `type`, `activated` FROM users WHERE username='$aun' AND password='$apw'")->fetch_row()) {
		$GLOBALS['aui']=$row[0];
		$GLOBALS['aut']=$row[1];
		$GLOBALS['aua']=$row[2];
		return true;
	} else {
		$GLOBALS['aun'] = "";
		$GLOBALS['apw'] = "";
		$GLOBALS['aui'] = 0;
		$GLOBALS['aut'] = 0;
		$GLOBALS['aua'] = 0;
		return false;
	}
}

function setSession() {
	$_SESSION["aun"] = $GLOBALS['aun'];
	$_SESSION["apw"] = $GLOBALS['apw'];
	$_SESSION["aui"] = $GLOBALS['aui'];
	$_SESSION["aut"] = $GLOBALS['aut'];
	$_SESSION["aua"] = $GLOBALS['aua'];
}

function clearSession() {
	session_destroy();
}

// setting session name to unique name (change if there is more than one blog on the site)
session_name("sb");
session_start();

if( checkPost() ) {
	if( verify() ) {
		setSession();
	} else {
		$_SESSION["notification"] = array('type'=>'danger','code'=>'not_wpwd');
		header("Location: ../");
		die();
	}
} else {
	checkSession();
}

if(!isset($aut))
	$aut=0;

?>