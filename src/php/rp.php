<?php

function compileStyling() {
    require_once "pp.php";

    $fileCustom = "../data/theme.json";
    $fileCss = "../css/main.scss";
    $fileOutput = "../data/custom-main.css";

    $settings = getJSONfile("../data/settings.json");
    $css = "";
    $fontFaceTemplate = '
@font-face {
    font-family: "{name}"; 
    src: url("../{url}");
}';
    if ($settings && $settings->fonts) {
        $css = arrayToTemplate($settings->fonts, $fontFaceTemplate);
    }

    $ofile = fopen($fileOutput, "w");

    $custom_data = getJSONfile($fileCustom);

    $cssfile = fopen($fileCss, "r");
    $css .= fread($cssfile, filesize($fileCss));
    fclose($cssfile);

    foreach ($custom_data as $key => $value) {
        $css = str_replace('$' . $key, $value, $css);
    }

    fwrite($ofile, $css);
    fclose($ofile);
}

require_once "auth.php";

require_once "ic.php";

if (!checkAction()) {
    die("Insufficient or incorrect input parameters.");
}

require_once "permAct.php";

if (!actionAllowed()) {
    $_SESSION['notification'] = array('type' => 'danger', 'code' => 'not_accden');
    header("Location: ../.");
    die();
}

/*
Global variables:
aun - username
apw - password
aui - user id
aut - user type
aua - activated
a   - action
+[input paramameters]
*/

/*
foreach ($GLOBALS as $key => $value) {
	if(isset($parameterType[$key]) || isset($parameterType["#".$key]))
		echo "$key: $value </br>";
}
*/
switch ($a) {
    case "li" :
        // auth.php has done all the work
        break;

    case "lo" :
        clearSession();
        break;

    case "ua" :
        require_once 'db.php';

        // i hope this is uncrackable
        $actCode = md5(time() - 100000);

        $con->query("INSERT INTO users (`username`,`password`,`email`,`actCode`,`lastChange`) VALUES ('$un', '$pw', '$em','$actCode',NOW())");

        require_once 'mail.php';
        sendActivationMail($un, $em, $actCode);
        $redirect = "manageUsers.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_ua');

        // Login as new user
        /*		$GLOBALS['aun']=$un;
                $GLOBALS['apw']=$pw;

                // get id
                verify();
        */
        // write data to session
//		setSession(); 
        break;

    case "uv" :
        require_once 'db.php';
        $con->query("UPDATE users SET activated=1 WHERE actCode='$ac'");
        if ($row = $con->query("SELECT `id`, `type`, `activated`, `username`, `password` FROM users WHERE actCode='$ac'")->fetch_row()) {
            $GLOBALS['aui'] = $row[0];
            $GLOBALS['aut'] = $row[1];
            $GLOBALS['aua'] = $row[2];
            $GLOBALS['aun'] = $row[3];
            $GLOBALS['apw'] = $row[4];
        }
        setSession();
        $redirect = "profile.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_uv');
        break;

    case "uu" :
        require_once 'db.php';
        $passSql = (isset($pw) ? ", password='$pw'" : "");
        $con->query("UPDATE users SET username='$un',email='$em'$passSql WHERE id=$uid");
        $aun = $un;
        setSession();
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_uu');
        $redirect = "profile.php";
        break;

    case "ur" :
        require_once 'db.php';

        $res = $con->query("SELECT type FROM users WHERE id='$uid'");

        if ($row = $res->fetch_array(MYSQLI_NUM)) {
            $type = $row[0];

            $res = $con->query("SELECT COUNT(id) FROM users WHERE type=2");
            if ($row = $res->fetch_array(MYSQLI_NUM)) {
                if ($type == 1 || $row[0] > 1) {
                    $con->query("DELETE FROM users WHERE id='$uid'");
                    $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_ur');

                    if ($uid == $aui) {
                        clearSession();
                    }
                } else {
                    $_SESSION['notification'] = array('type' => 'danger', 'code' => 'not_tfa');
                }
            }

        } //else no such user

        $redirect = "manageUsers.php";
        break;

    case "ut" :
        require_once 'db.php';

        $res = $con->query("SELECT type FROM users WHERE id=$uid");
        if ($row = $res->fetch_array(MYSQLI_NUM)) {
            $type = $row[0];

            $res = $con->query("SELECT COUNT(id) FROM users WHERE type=2");
            if ($row = $res->fetch_array(MYSQLI_NUM)) {
                if ($type == 1 || $row[0] > 1) {

                    $type = ($type % 2) + 1;
                    $con->query("UPDATE users SET type=$type WHERE id=$uid");

                    if ($uid == $aui) {
                        $aut = $type;
                        setSession();
                    }

                    $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_ut');
                } else {
                    $_SESSION['notification'] = array('type' => 'danger', 'code' => 'not_tfa');
                }
            }
        } //else no such user

        $redirect = "manageUsers.php";

        break;

    case "pa" :
        require_once 'db.php';
        $md = $md ? 'true' : 'false';
        $con->query("INSERT INTO posts (`uid`,`cid`,`title`,`text`,`markdown`,`postDate`) VALUES ($uid,$cid,'$ti', '$te', $md, NOW())");
        $redirect = "managePosts.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_pa');

        break;

    case "pr" :
        require_once 'db.php';
        $con->query("DELETE FROM posts WHERE id='$pid'");
        $redirect = "managePosts.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_pr');
        break;

    case "pu" :
        require_once 'db.php';
        $md = $md ? 'true' : 'false';
        $con->query("UPDATE posts SET title='$ti',text='$te', cid=$cid, uid=$uid, markdown=$md WHERE id=$pid");
        $redirect = "managePosts.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_pu');
        break;

    case "ca" :
        if (!isset($cid) || $cid == 0)
            $cid = "NULL";
        $con->query("INSERT INTO categories (name, parentId) VALUES ('$ti',$cid)");
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_ca');
        $redirect = "manageCategories.php";
        break;

    case "cr" :
        $con->query("DELETE FROM categories WHERE id=$cid");
        $redirect = "manageCategories.php";
        $_SESSION['notification'] = array('type' => 'success', 'code' => 'not_cr');
        break;

    case "fa" :
        $target_dir = "../data/files/";
        $target_file = $target_dir . basename($ti);
        $errCode = 0;

        if (file_exists($target_file))
            $errCode = "not_fiex";

        if ($_FILES["file"]["size"] > 5000000000) // 5Mb file size limit
            $errCode = "not_fitb";

        if (!$errCode) {
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                $_SESSION['notification'] = array("code" => "not_fa", "type" => "success");
            } else {
                $errCode = "not_fierr";
            }
        }
        if ($errCode)
            $_SESSION['notification'] = array("code" => $errCode, "type" => "danger");
        $redirect = 'manageFiles.php';
        break;

    case "fr" :
        $target_dir = "../data/files/";
        $target_file = $target_dir . basename($ti);

        if (file_exists($target_file)) {
            unlink($target_file);
            $_SESSION['notification'] = array("code" => "not_fr", "type" => "success");
        } else {
            $_SESSION['notification'] = array("code" => "not_fidex", "type" => "danger");
        }
        $redirect = 'manageFiles.php';
        break;

    case "au" :

        $fileCustom = "../data/theme.json";

        $cfile = fopen($fileCustom, "w");
        fwrite($cfile, $json);
        fclose($cfile);

        compileStyling();

        $redirect = "manageAppearance.php";
        break;

    case "su" :

        $fileSettings = "../data/settings.json";

        $file = fopen($fileSettings, "w");
        fwrite($file, $json);
        fclose($file);

        compileStyling();

        $redirect = "manageSettings.php";
        break;


}

if ($con->error) {
    echo $con->error;
} else {
    // Redirect back to website

    if (!isset($redirect)) {
        $redirect = '.';
    }

    header("Location: ../$redirect");
    die();
}


?>