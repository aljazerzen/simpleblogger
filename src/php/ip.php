<?php
require_once "php/auth.php"; 
require_once "php/ic.php";
require_once "php/permPage.php"; 
if(!checkPage()) {
	// secondary pages
	switch ($page) {
		case 'editPost':
			$page='newPost';
			break;
		default:
			// when there is no secondary page

			// redirect
			$redirect=".";
			switch ($page) {

			}
			header("Location: $redirect");
			die();	
	}
}

switch($page) {
	case 'editPost':
	case 'newPost':
	case 'managePosts':
	case 'manageCategories':
	case 'manageFiles':
	case 'manageAppearance':
	case 'manageSettings':
	case 'manageUsers':
	case 'profile':
		$controlPanel = true;
		break;
	default:
		$controlPanel = false;
}

require_once "php/pp.php"; 

$notification = "";
if(isset($_SESSION["notification"])) {
	if(!isset($_SESSION["notification"]["type"])) {
		$_SESSION["notification"]["type"]="info";
	}
	$notification = arrayToTemplate(array(array('text'=>$STRING[$_SESSION["notification"]['code']],'type'=>$_SESSION["notification"]['type'])), $notificationTemplate); 
	unset($_SESSION["notification"]);
}

?>