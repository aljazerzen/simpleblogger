<?php

function genCaptcha() {
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}
	include $_SERVER['DOCUMENT_ROOT']."/res/php-captcha/simple-php-captcha.php";

	$_SESSION['captcha'] = simple_php_captcha(array('fonts' => array('fonts/cheapink.ttf','fonts/deportee.ttf','fonts/fuente_dilana.ttf','fonts/times_new_yorker.ttf')));

	echo "<img src='".$_SESSION['captcha']['image_src']	."'></img>";
}

function checkCaptcha(){
	if (session_status() == PHP_SESSION_NONE) {
		session_start();
	}

	if(!isset($_POST['captcha']) || !isset($_SESSION['captcha']))
		return false;
	return $_POST['captcha']==$_SESSION['captcha']['code'];
}

?>	
