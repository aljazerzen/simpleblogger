<?php 

$actionPermissions = array(
'li' => array('p'),    	 				//  login
'lo' => array('p'),	     				//  logout
'ua' => array('p'),      				//  addUser
'uv' => array('p'),      				//  activateAccount
'uu' => array('uo', 'a'),				//  update user
'ur' => array('uo', 'a'),				//  remove user
'ut' => array('a'),						//  remove user
'pa' => array('ua'),      				//  add post
'pu' => array('ua'),					//  update post
'pr' => array('ua'),					//  remove post
'ca' => array('ua'),      				//  add category
'cr' => array('ua'),      				//  remove category
'fa' => array('ua'),      				//  upload file
'fr' => array('ua'),      				//  delete file
'au' => array('ua'),      				//  update appearance
'su' => array('ua'),      				//  update settings
	);

/**
 * Checks all the action permissions and checks if user has any of the granted 
 * @return true/false
 */
function actionAllowed() {
	require_once "perm.php";
	global $actionPermissions, $a;

	foreach ($actionPermissions[$a] as $permission) {
		if(hasPermission($permission)) {
			return true;
		}
	}

	return false;
}

?>