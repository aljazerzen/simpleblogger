<?php

/**
 * Determines whether user has perticular permission granted given global parameters
 * @param prc - permission code
 * @return true/false
 */
function hasPermission($pc) {
	include_once "db.php";
	global $con;

	switch($pc) {
		case "p":
			return true;
		case "a":
			return $GLOBALS['aut']==2;
		case "uo":
			// you have to be performing actions on yourself
			return $GLOBALS['aut']>=1 && $GLOBALS['aui']==$GLOBALS['uid'];
		case "ua":
			return $GLOBALS["aua"]==true;
	}
}

?>