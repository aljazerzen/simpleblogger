<?php

$con = null;
/*
Connects to mySQL server and saves connection in global variable con
*/
function dbConnect() {
    global $con;
    if (isset($con))
        return;


    $host = "127.0.0.1";
    $username = "root";
    $password = "";
    $database = "simpleBlogger";

    $con = mysqli_connect($host, $username, $password, $database);

    if (mysqli_connect_errno()) {
        echo "Connecting failed: " . mysqli_connect_error();
        die();
    }
    $con->set_charset("utf8");
}

dbConnect();


?>