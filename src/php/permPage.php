<?php 

$pagePermissions = array(
'homepage'            	=> 'p',
'post'					=> 'p',
'posts'					=> 'p',
'editPost'				=> 'u',
'newPost'				=> 'u',
'managePosts'			=> 'u',
'manageCategories'		=> 'u',
'manageFiles'			=> 'u',
'manageAppearance'		=> 'u',
'manageSettings'		=> 'u',
'manageUsers'			=> 'a',
'profile'				=> 'u',
);

/**
 * Checks if user has permission to access page given global parameters
 * @return true/false
 */
function pageAllowed() {
	global $pagePermissions, $page;

	if(!isset($pagePermissions[$page]))
		return false;

	// if permission is simple, dont run all the input checks
	switch($pagePermissions[$page]) {
		case "p":
			return $GLOBALS['aut']>=0;		
		case "u":
			return $GLOBALS['aut']>=1;		
		case "a":
			return $GLOBALS['aut']==2;	
		default :
			require_once "ic.php";
			if(!checkPage()) {
				die();
				// return false;
			}
			require_once "perm.php";
			return hasPermission($pagePermissions[$page]); 
	}

	return false;
}

if(!pageAllowed()) {
	header("Location: .");
	die("Forbidden");
}

?>