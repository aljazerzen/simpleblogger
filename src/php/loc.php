<?php

$STRING = array(
'not_pa'=>'Post successfully submitted',
'not_pr'=>'Post deleted',
'not_pu'=>'Post successfully updated',
'not_ca'=>'Category added',
'not_cr'=>'Category removed',
'not_ua'=>'User added',
'not_uv'=>'Your account has been activated',
'not_uu'=>'Profile successfully updated',
'not_ur'=>'User removed',
'not_ut'=>'User type toggled',
'not_fa'=>'File uploaded',
'not_fr'=>'File deleted',
'not_wpwd'=>'Wrong user name or password',
'not_accden'=>'You cannot do that... Stop tinkering around',
'not_fiex'=>'Cant upload file because it already exists',
'not_fitb'=>'Cant upload file because it is too big',
'not_fidex'=>'File does not exist',
'not_fierr'=>'Error while uploading file',
'not_tfa'=>'There must be at least one admin',
	);

$dateFormat = "DD/MM/YYYY";

$daysInWeek = array(array("Pon","Ponedeljek"),array("Tor","Torek"),array("Sre","Sreda"),array("Čet","Četrtek"),array("Pet","Petek"),array("Sob","Sobota"),array("Ned","Nedelja"));


?>