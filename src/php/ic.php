<?php

$parameterType = array(
    'a' => 'actionCode',            // action code
    'u' => 'username',            // username
    'p' => 'password',            // password
    'ac' => 'activationCode',        // activation code
    'uid' => 'id',                    // user id
    'pid' => 'id',                    // service id
    'cid' => 'id',                    // category id
    'ti' => 'plainText',            // title
    'te' => 'styledText',            // text
    'un' => 'username',            // new username
    'pw' => 'password',            // new password
    'em' => 'email',                // new email
    'md' => 'boolean',            // is markdown
    'fd' => 'time',                // from date
    'td' => 'time',                // to date
    'st' => 'number',                // start (offset of posts)
    'cn' => 'number',                // count (limit of posts)
    'json' => 'json',                // json string
    'oi' => 'number',                // ordering index
);

$reqParamsAct = array( // the ones starting with # are optional
    'li' => array(),                                                // login
    'lo' => array(),                                                // logout
    'ua' => array('un', 'pw', 'em'),                                    // addUser
    'uv' => array('ac'),                                            // activate account
    'uu' => array('uid', '#pw', 'un', 'em'),                            // update user
    'ur' => array('uid'),                                        // remove user
    'ut' => array('uid'),                                        // toggle admin
    'pa' => array('ti', 'te', 'cid', 'uid', '#md'),                    // add post
    'pu' => array('pid', 'ti', 'te', 'cid', 'uid', '#md'),            // update post
    'pr' => array('pid'),                                            // remove post
    'ca' => array('ti', '#cid'/*parent*/),                            // add category
    'cr' => array('cid'),                                        // remove category
    'fa' => array('ti'),                                            // add file
    'fr' => array('ti'),                                            // delete file
    'au' => array('json'),                                            // update appearance
    'su' => array('json'),                                            // update settings
);

$reqParamsPage = array(
    'post' => array('pid'),
    'posts' => array('ti', '#cid', '#uid', '#df', '#dt', '#st', '#cn', '#oi'),
    'editPost' => array('pid'),
    'newPost' => array(),
    'profile' => array(),
);

/*
 * checks if parameter is of correct type
 * @param pc parameter code
 * @param pv parameter value
 * @return checked parameter / false
 */
function validateParameter($pc, $pv) {
    global $parameterType, $reqParamsAct;

//	echo "$pc $pv $parameterType[$pc]<br/>";

    if (!isset($parameterType[$pc]))
        return false;

    switch ($parameterType[$pc]) {
        case 'actionCode' :
            if (isset($reqParamsAct[$pv]))
                return $pv;
            return false;
        case 'username' :
            $pv = preg_replace("/[^a-zA-Z.-_]/", "", $pv);

            if (!empty($pv) && strlen($pv) >= 3)
                return $pv;
            return false;

        case 'password' :
            // has to be 128 chars long
            // has to include only hex chars

            $pv = strtoupper($pv);

            if (strlen($pv) != 128)
                return false;

            if (preg_match("|[A-F0-9]*|", $pv) == 1)
                return $pv;

            return false;
        case 'activationCode' :
            if (strlen($pv) != 32)
                return false;

            if (preg_match("|[a-f0-9]*|", $pv) == 1)
                return $pv;

            return false;
        case 'id' :
            $pv = preg_replace("/[^0-9]/", "", $pv);

            if (!empty($pv) && $pv != 0)
                return $pv;

            return false;
        case 'email' :
            if (filter_var($pv, FILTER_VALIDATE_EMAIL))
                return $pv;
            return false;
        case 'phone' :

            $pv = preg_replace("/[^0-9]/", "", $pv);
            if (empty($pv))
                return false;

            $pv = sprintf("%011.11s", $pv);

            if (substr($pv, 0, 3) == "000")
                $pv = "386" . substr($pv, 3);

            return $pv;
        case 'styledText' :
            $pv = str_replace("Š", "&#352;", $pv);
            $pv = str_replace("š", "&#353;", $pv);
            $pv = str_replace("Ž", "&#381;", $pv);
            $pv = str_replace("ž", "&#382;", $pv);
            $pv = str_replace("Č", "&#268;", $pv);
            $pv = str_replace("č", "&#269;", $pv);
            return addslashes($pv);
        case 'plainText' :
            $pv = str_replace("Š", "&#352;", $pv);
            $pv = str_replace("š", "&#353;", $pv);
            $pv = str_replace("Ž", "&#381;", $pv);
            $pv = str_replace("ž", "&#382;", $pv);
            $pv = str_replace("Č", "&#268;", $pv);
            $pv = str_replace("č", "&#269;", $pv);
            $pv = preg_replace("/[^0-9a-zA-Z.,-:;?! ]/", "", $pv);
            if (!empty($pv))
                return $pv;
            return false;
        case 'time' :
            // years are restricted from 1000 - 2299 (lets hope that it is not enough)
            if (preg_match("|[1-2][0-2][0-9][0-9].[0-2]?[0-9].[0-9]?[0-9]|", $pv) == 1)
                return $pv;
            return false;
        case 'number':
            $pv = preg_replace("/[^0-9-]/", "", $pv);

            if (!empty($pv))
                return $pv;

            return false;
        case 'boolean':
            return $pv == true;
        case 'json':
            return $pv;
    }
    return false;
}

function validatePOST($pc) {
    if (isset($_POST[$pc])) {
        return validateParameter($pc, $_POST[$pc]);
    }
    return false;
}

function validateGET($pc) {
    if (isset($_GET[$pc])) {
        return validateParameter($pc, $_GET[$pc]);
    }
    return false;
}

/**
 * Checks POST inputs and determines if action has enough valid parameters
 * @return boolean
 */
function checkAction() {
    global $reqParamsAct, $parameterType;

    // action code
    global $a;
    $a = validatePOST('a');

    if ($a == false)
        return false;

    $fail = "";

    // check if all required parameters for this action are valid
    foreach ($reqParamsAct[$a] as $pc) {

        // parameter code trimmed (without #)
        $pct = ltrim($pc, "#");

        // validate parameter
        $GLOBALS[$pct] = validatePOST($pct);

        if ($GLOBALS[$pct] == false) {
            // parameter is invalid

            if ($pct == "uid" && $GLOBALS["aut"] >= 1) {
                // uid if current user is used
                $GLOBALS[$pct] = $GLOBALS["aui"];

            } else {
                if (substr($pc, 0, 1) == "#") {
                    // optional
                    $GLOBALS[$pct] = "";
                    if ($parameterType[$pct] == "id") {
                        $GLOBALS[$pct] = 0;
                    }

                } else {
                    // required -> stop, too few valid parameters
                    $fail = $pct;
                    break;
                }
            }
        }
    }

    // unsuccessful, unset parameters
    if ($fail != "") {
        echo $fail;
        foreach ($reqParamsAct[$a] as $pc) {
            unset($GLOBALS[$pc]);
        }
        $a = false;
    }

    return $a != false;
}

/**
 * Checks GET inputs and determines if page has enough valid parameters
 * @return boolean
 */
function checkPage() {
    global $page, $reqParamsPage;

    if (!isset($reqParamsPage[$page]))
        return true;

    // check if all reqired parameters for this action are valid
    foreach ($reqParamsPage[$page] as $pc) {

        // parameter code trimmed (without #)
        $pct = ltrim($pc, "#");

        // validate parameter
        if (!validateGET($pct)) {
            // if it is not optional
            if ($pct == $pc) {
                return false;
            }
        } else {
            $GLOBALS["$pct"] = $_GET["$pct"];
        }
    }

    return true;
}