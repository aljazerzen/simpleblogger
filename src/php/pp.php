<?php 

require 'loc.php';

$postTemplate = '<div class="post">
			<h1><a href="post.php?pid={id}">{title}</a></h1>

			<div class="content">
				{text}
			</div>
			
			<div class="post-info">
				<span class="date">{postDate}</span> 
				<span class="info-divider"> | </span> 
				<span class="author">{user}</span>
				<span class="info-divider"> | </span>
				<span class="category">Posted in: {categoryPath}</span>
				<div class="clear"></div>
			</div>
		</div>';

$notificationTemplate = '<div class="alertContainer">
			<div class="alert alert-{type} alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>{text}
			</div>
		</div>';

$seeMoreMessage = "...";

// Functions for getting data out of the database or some other place	

function dbGetProfile() {
	require_once "db.php";
	global $con, $aui;
	return $con->query("SELECT username, email FROM users WHERE id=$aui");
}

function dbGetUserList() {
	require_once "db.php";
	global $con, $aui;
	return $con->query("SELECT id, username, email, type FROM users");
}

function dbGetPost($pid) {
	require_once "db.php";
	global $con;
	return $con->query("SELECT p.id as id, p.title as title, p.text as text, p.markdown as markdown, p.postDate as postDate, u.username as user, c.name as category, p.cid as categoryPath FROM posts p INNER JOIN users u ON u.id=p.uid INNER JOIN categories c ON c.id=p.cid WHERE p.id=$pid");
}

function dbGetPosts() {
	require_once "db.php";
	global $con;
	return $con->query("SELECT p.id as id, p.title as title, p.text as text, p.markdown as markdown, p.postDate as postDate, u.username as user, c.name as category, p.cid as categoryPath FROM  posts p INNER JOIN users u ON u.id=p.uid INNER JOIN categories c ON c.id=p.cid ");
}

function dbGetMasterCategories() {
	require_once "db.php";
	global $con; 
	return $con->query("SELECT id, name FROM categories WHERE parentId IS NULL");
}

function dbGetCategoryList() {
	require_once "db.php";
	global $con; 
	return $con->query("SELECT id, parentId, name FROM categories");
}

function dbGetCategoryListOf($parentId) {
	require_once "db.php";
	global $con; 
	return $con->query("SELECT id, name FROM categories WHERE parentId=$parentId");
}

// formatting all done
/*
Ordering:
|        Comment        | orderingId |
|-----------------------|------------|
| Newest first          |          1 |
| Oldest first          |          2 |
| Last changed first    |          3 |
| Last changed last     |          4 |
| Alphabetical          |          5 |
| Alphabetical reversed |          6 |
 */
function postQuery($shorten = true) {
	global $con, $postTemplate, $seeMoreMessage;
	global $cid, $uid, $df, $dt, $st, $cn, $oi;
	require_once "db.php";

	$filterSql = "";
	if($cid) {
		$filterSql.="AND p.cid=$cid ";
	}
	if($uid) {
		$filterSql.="AND p.uid=$uid ";
	}
	if($df) {
		$filterSql.="AND p.postDate>'$df' ";
	}
	if($dt) {
		$filterSql.="AND p.postDate<='$dt' ";
	}
	switch($oi) {
		case 1:
			$filterSql.="ORDER BY p.postDate DESC ";
			break;
		case 2:
			$filterSql.="ORDER BY p.postDate ASC ";
			break;
		case 3:
			$filterSql.="ORDER BY p.lastChange DESC ";
			break;
		case 4:
			$filterSql.="ORDER BY p.lastChange ASC ";
			break;
		case 5:
			$filterSql.="ORDER BY p.title ASC ";
			break;
		case 6:
			$filterSql.="ORDER BY p.title DESC ";
			break;
	}
	if($st || $cn) {
		$filterSql.="LIMIT ";
		if($cn) {
			$filterSql.="$cn ";
		}
		if($st) {
			$filterSql.="OFFSET $st ";
		}
	}

	$res = $con->query("SELECT p.id as id, p.title as title, p.text as text, p.markdown as markdown, p.postDate as postDate, u.username as user, c.name as category, p.cid as categoryPath FROM posts p, users u, categories c WHERE u.id=p.uid AND c.id=p.cid $filterSql");

	$postArray = formatArray(resToArray($res));

	if($shorten) {
		return arrayToTemplate(shortenPosts($postArray, 80, $seeMoreMessage) ,$postTemplate);
	} else {
		return arrayToTemplate($postArray,$postTemplate);
	}

}

function getFileList() {
	$dir = "data/files/";
	$list = scandir($dir);
	$r=array();
	$i=0;
	foreach ($list as $value) {
		if(substr($value,0,1)!='.') {
			$r[$i++] = array('name' => $value, 'path' => $dir.$value);
		}
	}
	return $r;
}

function getJSONfile($path, $assoc=false) {
	$file = fopen($path, "r");
	$data = json_decode(fread($file,filesize($path)), $assoc);
	fclose($file);
	return $data;
}

// Functions for formatting data

/*function arrayToTable($array, $keyBindings, $tableId="") {
	if($tableId!="") 
		$tableId = " id='$tableId'";

	$filedsToPrint = array (
		"fairName" => "Fair name",
		"blockId" => "Block number",
		"startDate" => "Start date",
		"endDate" => "End date"
		);

	$r='<table class="table"$tableId>';
	//thead
	$r.='<thead><tr>';
	foreach ($keyBindings as $key => $value) 
		$r.="<th>".$value."</th>";
	
	//body
	$r.='</tr></thead><body>';
	foreach ($array as $key => $value) {
		$r.="<tr class='table-row' name='$key'>";
		foreach ($filedsToPrint as $key2 => $value2) {
			$r.="<td> $value[$key2]</td>";
		}
		$r.="</tr>";
	}
	$r.='</table>';
	return $r;
}*/

/**
 * Converts mysqli_result to html table with id $tableId and field names from $keyBindings
 * Every row has name of
 * $keyBindings = array ("name in mySql result" => "column name");
 * @return html table
 */
function resToTable($res, $keyBindings, $tableId="", $rowName="") {
	if(!($fields = $res->fetch_fields()))
		return "<strong>Invaild sql result</strong>";
	
	if($tableId!="") 
		$tableId = " id='$tableId'";


	$r='<table class="table"$tableId>';
	//thead
	$r.='<thead><tr>';
	foreach ($fields as $key => $value) {
		if(array_key_exists($value->name, $keyBindings)) {
			$r.="<th>".$filedsToPrint[$value->name]."</th>";
		}
	}
	//body
	$r.='</tr></thead><tbody>';
	while($row = $res->fetch_array()) {
		$r.="<tr class='table-row'";
		if($rowName!="") 
			$r.=" name='$row[$rowName]'";
		$r.=">";
		foreach($keyBindings as $key => $value) {
			if(array_key_exists($key, $row)) {
				$r.="<td>$row[$key]</td>";
			}
		}
		$r.="</tr>";
	}
	$r.="</tbody></table>";
	return $r;
}

function arrayToTemplate($array, $template, $selTemplate="", $selValue=NULL, $selIndex=NULL) {
	$r="";
	$template = str_replace(array("\t","\n"),"",$template);

	foreach($array as $value) {
		$row=$template;
		foreach ($value as $key => $val) {
			$row=str_replace("{".$key."}",$val,$row);
		}
		if($selIndex && $value[$selIndex] == $selValue) {
			$row=str_replace("{selected}",$selTemplate,$row);
		} else {
			$row=str_replace("{selected}","",$row);
		}
		$r.=$row;
	}
	return $r;
} 

function addPreviewToImages($files, $previewTemplate, $onlyImages=false) {

	$extsToPreview = array('jpg', 'jpeg', 'gif', 'svg', 'ico', 'png');
	foreach ($files as $key => $file) {
		$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
		if( array_search($ext, $extsToPreview) !== false) {
			$files[$key]["preview"] = str_replace("{src}",$file["path"],$previewTemplate);
		} else {
			if($onlyImages) {
				unset($files[$key]);
			} else {
				$files[$key]["preview"] = "";
			}
		}
	}
	return $files;
}

function filterFiles($files, $allowedFormats) {
	foreach ($files as $key => $file) {
		$ext = pathinfo($file['name'],PATHINFO_EXTENSION);
		if( array_search($ext, $allowedFormats) === false) {
			unset($files[$key]);
		}
	}
	return $files;	
}

/**
 * Formats $array into html option objects
 * @param value index of $array to use as a value
 * @param label index of $array to use as a label attribute
 * @param selected if it matches row['$selIndex'] the option element has selected attribute attached
 * @param selIndex 
 */
function arrayToOptions($array, $value, $label, $selected=0, $selIndex=NULL) {
	$r="";
	if($selIndex===NULL)
		$selIndex=$label;
	foreach($array as $row) {
		$r.="<option value='{$row["$value"]}'".($row["$selIndex"]==$selected?" selected":"").">{$row["$label"]}</option>";
	}
	return $r;
}

/**
 * Formats $res into html option objects
 * @param value index of $res to use as a value
 * @param label index of $res to use as a label attribute
 * @param selected if it matches row['$label'] the option has selected attribute attached
 * @param selIndex 
 */
function resToOptions($res, $value, $label, $selected, $selIndex=NULL) {
	$r="";
	if($selIndex===NULL)
		$selIndex=$label;

	while($row = $res->fetch_assoc()) {
		$r.="<option value='{$row["$value"]}'".($row["$selIndex"]==$selected?" selected":"").">{$row["$label"]}</option>";
	}
	return $r;
}

function resToArray($res) {
	if($res===false || !($fields = $res->fetch_fields()))
		return array();

	$r = array();
	for($cnt=0;$row = $res->fetch_array();$cnt++) {
		$r[$cnt]=array();
		foreach ($fields as $key => $value) {
			$r[$cnt][$value->name]= $row[$value->name];
		}
	}
	return $r;
}

// Functions for changing format of individual fields eg. phone number spacing

function formatArray($array) {
	global $costUnits;
	foreach($array as $i=>$iv) {
		foreach($array[$i] as $key => $value) {
			$array[$i][$key]=formatField($key,$value, $array[$i]);
		}
	}
	return $array;
}

function formatField($type, $val, $arRow=NULL) {
	require_once "db.php";
	global $con, $costUnits, $daysInWeek;


	switch($type) {
		case 'categoryPath':
			$categories = indexesToCids(resToArray(dbGetCategoryList()));
			$val = getCategoryPath($categories[$val],$categories);
			break;
		case 'phone':
			$val=substr_replace($val, " ", 8, 0);
			$val=substr_replace($val, " ", 5, 0);
			$val="+".substr_replace($val, " ", 3, 0);
			break;
		case 'type':
			if($val==1)
				$val='User';
			else
				$val='Admin';
			break;
		case 'text':
			if($arRow && $arRow['markdown']==true) {
				require_once 'Parsedown.php';
				$Parsedown = new Parsedown();
				$val = $Parsedown->text( stripslashes(htmlspecialchars_decode($val))); 

				$val = str_replace("<table>", "<table class='table'>", $val);
			}
			break;
	}
	return $val;
}

function indexesToCids($categories){
	// change indexes to cids
	$newC = array();
	foreach ($categories as $key=>$category) {
		$newC[$category['id']] = $category;
	}	
	return $newC;
}

function getCategoryPath($category, $categories) {
	if(!isset($category['path'])) 
		if($category['parentId']==0)
			$category['path'] = $category['name'];
		else
			$category['path'] = getCategoryPath($categories[$category['parentId']],$categories) . ' > ' .	 $category['name'];
	return $category['path'];
}

function categoriesToFullPath($categories) {
	foreach ($categories as $key=>$category) {
		$categories[$key]['path'] = getCategoryPath($categories[$key], $categories);
	}
	return $categories;
}

function toCategoryPathArray($categories, $cid) {
	$r = array();
	for($i=0; $cid!=0; $i++) {
		$r[$i] = $categories[$cid];
		$cid = $categories[$cid]['parentId'];
	}
	return $r;
}

function shortenPosts($posts, $length, $append) {
	foreach ($posts as $key=>$post) {

		$postLenght = strlen($post['text']);
		$tagsLenght=0;
		$tags = array();
		$tagStart=-1;
		$cutAt=0;
		$textToShow = $length;

		for($i=0; $i < $postLenght && $textToShow>0; $i++) {
			if($tagStart==-1 && $post['text'][$i]=='<') {
				$tagStart=$i;
			}
			// echo "processing: " . $post['text'][$i] . " " . $tagStart."\n";
			if($tagStart!=-1 && $post['text'][$i]=='>') {
				if($post['text'][$tagStart+1]=='/') {
					$tag = preg_replace("|\s.*|i", "", substr($post['text'], $tagStart+2, $i - $tagStart - 2));
					while($tagsLenght>0 && $tag != $tags[--$tagsLenght]) {
						// echo "Closed tag: $tags[$tagsLenght]\n";
					}
				} else {
					$tag = preg_replace("|\s.*|i", "", substr($post['text'], $tagStart+1, $i - $tagStart - 1));
					$tags[$tagsLenght++] = $tag;
					// echo "Added tag: $tag\n";
				}
				$tagStart=-1;
				$cutAt=$i;
			}

			if(ctype_space($post['text'][$i]) && $tagStart==-1) {
				$cutAt = $i;
				$textToShow--;
			} 
		}

		$posts[$key]['text'] = substr($post['text'],0,$cutAt);

		if(preg_replace("|\s|i", "", substr($post['text'], $cutAt))!="")
			$posts[$key]['text'] .= $append;


		while($tagsLenght) {
			$posts[$key]['text'] .= "</".$tags[--$tagsLenght].">";
		}
	}
	return $posts;
}

?>