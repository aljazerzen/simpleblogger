<?php $page="posts"; require_once "php/ip.php"; ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<?php include "snp/postHeader.php" ?>	
	<title><?php echo $ti." - ".$settings['blogName'];?></title>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">
<?php
include "snp/categoryBar.php";

echo postQuery(isset($_GET['shorten']));
?>
	</div>

	<?php include "snp/footer.php" ?>

</body>
</html>