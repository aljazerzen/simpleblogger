<?php 
$page="profile";
require_once "php/ip.php"; 

$profile=resToArray(dbGetProfile($aui))[0];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Profile - <?php echo $settings['blogName'];?></title>
	<link rel="stylesheet" type="text/css" href="css/profile.css">
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">
		
		<?php include "snp/editNavbar.php" ?>		

		<form class="col-sm-6 form-horizontal" action="php/rp.php" method="POST" id="profileForm">
			<input type="hidden" name="a" value="uu"> 

			<div class="form-group">
				<label for="inp-un" class="col-sm-4 control-label">Username</label>
				<div class="col-sm-8">
					<input type="text" class="form-control dataNew" id="inp-un" name="un" placeholder="Username" value="<?php echo $profile['username']?>"> 
					<p class="form-control-static dataOld" id="inp-un-static"><?php echo $profile['username']?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="inp-email" class="col-sm-4 control-label">Email</label>
				<div class="col-sm-8">
					<input type="text" class="form-control dataNew" id="inp-email" name="em" placeholder="Email" value="<?php echo $profile['email']?>">
					<p class="form-control-static dataOld" id="inp-email-static"><?php echo $profile['email']?></p>
				</div>
			</div>
			<div class="form-group">
				<label for="inp-pass" class="col-sm-4 control-label">Password</label>
				<div class="col-sm-8">
					<input type="password" class="form-control dataNew" id="inp-pass" name="pw" placeholder="Password" value="">
					<p class="form-control-static dataOld" id="inp-pass-static">***</p>
				</div>
			</div>
		</form>
		<div class="col-sm-3" id="actionBar">
			<div class="collapse in" id="editButton">
				<div class="btn btn-default margin" onclick="editProfile()">Edit</div>
			</div>
			<div class="collapse" id="cancelButton">
				<div class="btn btn-default margin" onclick="cancelEdit()">Cancel</div>
			</div>
			<div class="collapse margin" id="saveButton" onclick="saveProfile()">
				<div class="btn btn-default">Save</div>
			</div>
		</div>

	</div>

	<?php include "snp/footer.php" ?>
	<script src="js/profile.js"></script>

</body>
</html>
