<?php $page="manageCategories"; require_once "php/ip.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include "snp/header.php"; ?>
	<title>Categories - <?php echo $settings['blogName'];?></title>
</head>
<body>
	<?php include "snp/navbar.php" ?>

	<div class="container">

		<?php include "snp/editNavbar.php" ?>		

		<div class="col-sm-9">

			<table class="table table-hover">
				<thead>
					<tr><th>Name</th><th>Path</th><th>Delete</th></tr>
				</thead>
				<tbody>
					<?php echo arrayToTemplate(categoriesToFullPath(indexesToCids(resToArray(dbGetCategoryList()))),'
					<tr class="postRow" value="{id}" title="{name}"><td>{name}</td><td>{path}</td><td class="action-cell"><a class="btn btn-default glyphicon glyphicon-remove"></a></td></tr>');?>
				</tbody>
			</table>

			<div class="panel panel-default">
				<div class="panel-heading">
					<h1 class="panel-title">Add new category</h1>	
				</div>
				<div class="panel-body">
					<form class="form-inline" action="php/rp.php" method="POST">
						<input type="hidden" name="a" value="ca"/>

						<div class="form-group">
							<label for="categoryName" class="control-label">Name</label>
							<input type="text" id="categoryName" name="ti" class="form-control"/>
						</div>
						<div class='form-group'>
							<label for='parent' class="control-label">Parent category</label>
							<select class='form-control' name="cid" id="parent">
								<option value="">No parent</option>
								<?php 
								echo arrayToOptions(categoriesToFullPath(indexesToCids(resToArray(dbGetCategoryList()))),'id','path',$post['category']);
								?>
							</select>
						</div>
						<div class="form-group">
							<input type="submit" class="form-control" value="Add"/>
						</div>
					</form>
				</div>
			</div>

		</div>
	</div>

	<form class="hidden" action="php/rp.php" method="POST" id="deleteForm"> 
		<input type="hidden" name="a" value="cr"/>
		<input type="hidden" name="cid" id="cid"/>
	</form>

	<?php include "snp/footer.php" ?>
	<script src="js/manageCategories.js"></script>

</body>
</html>